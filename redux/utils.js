export const removeById = (collection = [], id) =>
  collection.filter(item => item.id !== id)

export const getById = (collection = [], id) =>
  collection.find(item => item.id === id)

export const updateById = (collection = [], id, newItem) =>
  collection.map(item => (item.id === id ? newItem : item))
