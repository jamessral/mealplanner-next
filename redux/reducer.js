import { combineReducers } from 'redux'
import ingredientsReducer from './modules/ingredients/reducer'
import mealPlansReducer from './modules/mealplans/reducer'
import recipesReducer from './modules/recipes/reducer'
import usersReducer from './modules/users/reducer'

export default combineReducers({
  ingredients: ingredientsReducer,
  mealPlans: mealPlansReducer,
  recipes: recipesReducer,
  users: usersReducer,
})
