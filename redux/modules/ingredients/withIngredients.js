import { connect } from 'react-redux'
import { getIngredients } from './selectors'

const mapStateToProps = state => ({
  ingredients: getIngredients(state),
})

export default WrappedComponent =>
  connect(
    mapStateToProps,
    {}
  )(WrappedComponent)
