import { connect } from 'react-redux'
import { getAuthToken } from './selectors'

const mapStateToProps = state => ({
  authToken: getAuthToken(state),
})

export default WrappedComponent =>
  connect(
    mapStateToProps,
    {}
  )(WrappedComponent)
