module.exports = {
    env: {
        browser: true,
        es6: true,
        jest: true,
        node: true
    },
    extends: [
        "eslint:recommended",
        "plugin:import/errors",
        "plugin:import/warnings",
        "plugin:react/recommended",
        "plugin:prettier/recommended"
    ],
    parser: "babel-eslint",
    parserOptions: {
        ecmaFeatures: {
            "jsx": true
        },
        ecmaVersion: 2018,
        sourceType: "module"
    },
    plugins: [
      "import",
      "babel",
      "jest",
      "react",
      "prettier"
    ],
    rules: {
        "linebreak-style": [
            "error",
            "unix"
        ],
        "no-console": "warn",
        "prettier/prettier": "error"
    }
}
