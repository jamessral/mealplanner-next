import React from 'react'
import SignUp from '../components/SignUp'

const SignUpPage = () => <SignUp />
SignUpPage.displayName = 'SignUpPage'

export default SignUpPage
