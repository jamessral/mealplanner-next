import React from 'react'
import RecipesForm from '../components/RecipesForm'
import RecipesList from '../components/RecipesList'

export default () => (
  <>
    <RecipesForm />
    <RecipesList />
  </>
)
