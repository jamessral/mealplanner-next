import React from 'react'
import SignIn from '../components/SignIn'

const SignInPage = () => <SignIn />

SignInPage.displayName = 'SignInPage'

export default SignInPage
