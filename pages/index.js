import React from 'react'

const IndexPage = () => <h2>Welcome to the Mealplanner</h2>
IndexPage.displayName = 'IndexPage'

export default IndexPage
