import React from 'react'
import IngredientsForm from '../components/IngredientsForm'
import IngredientsList from '../components/IngredientsList'

const Ingredients = () => (
  <>
    <IngredientsForm />
    <IngredientsList />
  </>
)

export default Ingredients
