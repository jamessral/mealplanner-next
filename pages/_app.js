import React from 'react'
import { Provider } from 'react-redux'
import App, { Container } from 'next/app'
import { getCookie, setCookie } from '../utils/cookies'
import Layout from '../components/Layout'
import store from '../redux/store'
import '../index.css'

import 'cross-fetch/polyfill'
const credentials =
  process.env.NODE_ENV === 'development' ? 'same-origin' : 'include'

export default class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }
    const cookie = getCookie('authToken', ctx.req)
    if (cookie && cookie !== 'undefined') {
      setCookie('authToken', cookie)
    }
    return { ...pageProps, isLoggedIn: Boolean(cookie) }
  }

  render() {
    const { Component, pageProps, isLoggedIn } = this.props
    return (
      <Provider store={store}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Provider>
    )
  }
}
