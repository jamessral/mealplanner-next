import React from 'react'
import PropTypes from 'prop-types'
import Router from 'next/router'
import Link from 'next/link'
import { connect } from 'react-redux'
import SubmitButtons from '../../components/SubmitButtons'
import { signIn } from '../../redux/modules/users/thunks'

const initialState = {
  email: '',
  password: '',
}

class SignInForm extends React.PureComponent {
  static propTypes = {
    signIn: PropTypes.func.isRequired,
  }

  state = initialState

  get canSubmit() {
    const { email, password } = this.state
    const emailValid = email && email.length > 3
    const passwordValid = password && password.length > 3

    return Boolean(emailValid && passwordValid)
  }

  handleCancel = event => {
    event.preventDefault()
    this.setState({ ...initialState })
  }

  handleFieldChange = event => {
    event.preventDefault()
    const name = event.target.name
    const value = event.target.value

    this.setState({ [name]: value })
  }

  handleSubmit = event => {
    const { email, password } = this.state
    event.preventDefault()
    this.props.signIn(email, password)
    Router.push('/')
  }

  render() {
    const { email, password } = this.state

    return (
      <div className="form-container">
        <h3 className="pb-4">Sign In</h3>
        <form>
          <label htmlFor="email">Email</label>
          <input
            className="form-field"
            type="email"
            name="email"
            value={email}
            onChange={this.handleFieldChange}
          />
          <label htmlFor="password">Password</label>
          <input
            className="form-field"
            type="password"
            name="password"
            value={password}
            onChange={this.handleFieldChange}
          />
          <SubmitButtons
            onCancel={this.handleCancel}
            onSubmit={this.handleSubmit}
            disabled={!this.canSubmit}
          />
        </form>
        <div className="flex flex-row justify-between items-center mt-4">
          <span>{"Don't have an account?"}</span>
          <Link href="/sign_up">
            <a className="text-blue text-base font-bold">Sign Up</a>
          </Link>
        </div>
      </div>
    )
  }
}

const mapStateToProps = () => ({})
const mapDispatchToProps = {
  signIn,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignInForm)
