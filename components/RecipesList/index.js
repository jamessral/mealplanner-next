import { compose } from 'redux'
import { connect } from 'react-redux'
import { getRecipes, getIsLoading } from '../../redux/modules/recipes/selectors'
import { fetchRecipes, updateRecipe } from '../../redux/modules/recipes/thunks'
import withAuthToken from '../../redux/modules/users/withAuthToken'
import RecipesList from './RecipesList'

const mapStateToProps = state => ({
  recipes: getRecipes(state),
  loading: getIsLoading(state),
})

const mapDispatchToProps = {
  fetchRecipes,
  updateRecipe,
}

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withAuthToken
)(RecipesList)
