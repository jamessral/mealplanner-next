import { connect } from 'react-redux'
import {
  beginEditRecipe,
  removeRecipe,
  updateRecipe,
} from '../../../redux/modules/recipes/thunks'
import RecipeItem from './RecipeItem'

const mapStateToProps = state => ({})

const mapDispatchToProps = {
  beginEditRecipe,
  removeRecipe,
  updateRecipe,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipeItem)
