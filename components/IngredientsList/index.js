import { compose } from 'redux'
import { connect } from 'react-redux'
import {
  getIngredients,
  getIsLoading,
} from '../../redux/modules/ingredients/selectors'
import {
  fetchIngredients,
  updateIngredient,
} from '../../redux/modules/ingredients/thunks'
import withAuthToken from '../../redux/modules/users/withAuthToken'
import IngredientsList from './IngredientsList'

const mapStateToProps = state => ({
  ingredients: getIngredients(state),
  loading: getIsLoading(state),
})

const mapDispatchToProps = {
  fetchIngredients,
  updateIngredient,
}

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withAuthToken
)(IngredientsList)
