import { connect } from 'react-redux'
import {
  beginEditIngredient,
  removeIngredient,
  updateIngredient,
} from '../../../redux/modules/ingredients/thunks'
import IngredientItem from './IngredientItem'

const mapStateToProps = () => ({})

const mapDispatchToProps = {
  beginEditIngredient,
  removeIngredient,
  updateIngredient,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IngredientItem)
