import React from 'react'
import PropTypes from 'prop-types'
import Head from 'next/head'
import Router from 'next/router'
import Navbar from './Navbar'
import '../index.css'

class Layout extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
    isLoggedIn: PropTypes.bool,
  }

  componentDidMount() {
    if (!this.props.isLoggedIn) {
      Router.push('/')
    }
  }

  render() {
    const { children, isLoggedIn } = this.props
    return (
      <div className="container-sm w-screen">
        <Head>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <Navbar isLoggedIn={isLoggedIn} />
        {children}
      </div>
    )
  }
}
export default Layout
