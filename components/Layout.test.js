import React from 'react'
import { render } from 'react-testing-library'
import Layout from './Layout'

it('renders without crashing', () => {
  const { container } = render(<Layout />)
  expect(container).toBeTruthy()
})
