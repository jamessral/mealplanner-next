import { compose } from 'redux'
import { connect } from 'react-redux'
import { withAuthToken } from '../../redux/modules/users'
import {
  thunks as ingredientThunks,
  withIngredients,
} from '../../redux/modules/ingredients'
import {
  selectors as recipeSelectors,
  thunks as recipeThunks,
} from '../../redux/modules/recipes'
import RecipesForm from './RecipesForm'

const mapStateToProps = state => ({
  isEditing: recipeSelectors.getIsEditing(state),
})

const mapDispatchToProps = {
  fetchIngredients: ingredientThunks.fetchIngredients,
  createRecipe: recipeThunks.createRecipe,
  finishEditRecipe: recipeThunks.finishEditRecipe,
  updateRecipe: recipeThunks.updateRecipe,
}

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withAuthToken,
  withIngredients
)(RecipesForm)
