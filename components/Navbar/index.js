import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { connect } from 'react-redux'
import { signOut } from '../../redux/modules/users/thunks'
import { getIsSignedIn } from '../../redux/modules/users/selectors'

class Navbar extends React.PureComponent {
  static propTypes = {
    isSignedIn: PropTypes.bool,
    signOut: PropTypes.func.isRequired,
  }

  handleSignOut = event => {
    event.preventDefault()
  }

  render() {
    const { isSignedIn } = this.props
    return (
      <div className="bg-teal px-6 py-4 flex flex-wrap justify-between">
        <div className="flex flex-1 items-center text-white pr-6 pr-8">
          <Link href="/">
            <a className="home-link">MealPlanner</a>
          </Link>
        </div>
        {isSignedIn && (
          <div className="flex flex-1 items-center justify-between p-0">
            <Link href="/ingredients">
              <a className="text-teal-lighter hover:text-white">Ingredients</a>
            </Link>
            <Link href="/recipes">
              <a className="text-teal-lighter hover:text-white">Recipes</a>
            </Link>
            <Link href="/mealplans">
              <a className="text-teal-lighter hover:text-white">MealPlans</a>
            </Link>
            <button className="link" onClick={this.handleSignOut}>
              Sign Out
            </button>
          </div>
        )}
        {!isSignedIn && (
          <Link href="/sign_in">
            <a className="link">Sign In</a>
          </Link>
        )}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isSignedIn: getIsSignedIn(state),
})

const mapDispatchToProps = {
  signOut,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar)
