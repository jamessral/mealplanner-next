


import SignUp from './SignUp'

const mapStateToProps = state => ({
  signUpError: userSelectors.getSignUpError(state),
})

const mapDispatchToProps = {
  signUp,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp)
